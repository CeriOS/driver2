//
//  ViewController.swift
//  iOS_Driver2
//
//  Created by Rey Cerio on 2017-10-03.
//  Copyright © 2017 Rey Cerio. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation

class MainScreen: UIViewController, CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager()
    var loginState: Bool = false
    var status = "Offline"
    var values = [String: Any]()
    
    let emailTextField: UITextField = {
        let tf = UITextField()
        tf.keyboardType = .emailAddress
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        tf.placeholder = "email"
        tf.borderStyle = .roundedRect
        tf.layer.cornerRadius = 6.0
        tf.layer.masksToBounds = true
        return tf
    }()
    
    let truckIdTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "truck Id"
        tf.borderStyle = .roundedRect
        tf.layer.cornerRadius = 6.0
        tf.layer.masksToBounds = true
        return tf
    }()
    
    let passwordTextField: UITextField = {
        let tf = UITextField()
        tf.isSecureTextEntry = true
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        tf.placeholder = "password"
        tf.borderStyle = .roundedRect
        tf.layer.cornerRadius = 6.0
        tf.layer.masksToBounds = true
        return tf
    }()
    
    lazy var loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Login", for: .normal)
        button.backgroundColor = .clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.gray.cgColor
        button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return button
    }()
    
    let onlineLabel: UILabel = {
        let label = UILabel()
        label.text = "Offline"
        label.textColor = .red
        return label
    }()
    
    lazy var onlineSwitch: UISwitch = {
        let onlineSwitch = UISwitch()
        onlineSwitch.isHidden = true
        onlineSwitch.addTarget(self, action: #selector(handleOnlineSwitch), for: .valueChanged)
        return onlineSwitch
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        setupViews()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("locations array in index 0: \(locations[0])")
        
        for i in locations {
            print("lat: \(i.coordinate.latitude), lng: \(i.coordinate.longitude)")
        }
        
        let userLocation: CLLocation = locations[0]
        let latitude = String(describing: userLocation.coordinate.latitude)
        let longitude = String(describing: userLocation.coordinate.longitude )
        let speed = String(describing: userLocation.speed )
        let date = String(describing: userLocation.timestamp)
        
        let convertDate = userLocation.timestamp
        let timestamp = convertDate.timeIntervalSince1970
        
        status = "Online"
        values = ["latitude": latitude, "longitude":longitude, "timestamp": timestamp, "speed": speed, "date": date, "status": status  ]
        saveLocationToFirebase(values: values)
        saveLocationToFirebaseWithAutoId(values: values)
        
    }
    
//    func checkIfTruckIdExists() {
//        guard let uid = Auth.auth().currentUser?.uid else {return}
//        guard let truckId = truckIdTextField.text else {return}
//
//        let truckIdRef = Database.database().reference().child("Cer_TruckId").child(uid).child(truckId)
//        truckIdRef.observeSingleEvent(of: , with: <#T##(DataSnapshot) -> Void#>, withCancel: <#T##((Error) -> Void)?##((Error) -> Void)?##(Error) -> Void#>)
//
//        let databaseRef = Database.database().reference().child("Cer_TruckId").child(uid)
//        databaseRef.updateChildValues([truckId: "Online"])
//    }
    
    func saveLocationToFirebase(values: [String: Any]) {
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let truckId = truckIdTextField.text else {return}
        let databaseRef = Database.database().reference().child("Cer_Driver_Location").child(uid).child(truckId)
        databaseRef.updateChildValues(values)
    }
    
    func removeEntryInOverwriteLocations() {
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let truckId = truckIdTextField.text else {return}
        let databaseRef = Database.database().reference().child("Cer_Driver_Location").child(uid).child(truckId)
        databaseRef.removeValue()
    }
    
    func updateOnlineStatus() {
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let truckId = truckIdTextField.text else {return}
        let databaseRef = Database.database().reference().child("Cer_Driver_Location").child(uid).child(truckId)
        //change value of [status:online] to [status:offline]
        databaseRef.updateChildValues(["status": "Offline"])
    }
    
    
    func saveLocationToFirebaseWithAutoId(values: [String: Any]) {
        guard let uid = Auth.auth().currentUser?.uid else {return}
        guard let truckId = truckIdTextField.text else {return}
        let databaseRef = Database.database().reference().child("Cer_Driver_Saved_Location").child(uid).child(truckId).childByAutoId()
        databaseRef.updateChildValues(values)
    }
    
    func setupViews() {
        
        view.addSubview(emailTextField)
        view.addSubview(truckIdTextField)
        view.addSubview(passwordTextField)
        view.addSubview(loginButton)
        view.addSubview(onlineLabel)
        view.addSubview(onlineSwitch)
        
        view.addConstraintsWithVisualFormat(format: "H:|-16-[v0]-16-|", views: emailTextField)
        view.addConstraintsWithVisualFormat(format: "H:|-16-[v0]-16-|", views: passwordTextField)
        view.addConstraintsWithVisualFormat(format: "H:|-16-[v0]-16-|", views: truckIdTextField)
        view.addConstraintsWithVisualFormat(format: "H:|-140-[v0(100)]", views: loginButton)
        view.addConstraintsWithVisualFormat(format: "H:|-160-[v0]", views: onlineLabel)
        view.addConstraintsWithVisualFormat(format: "H:|-160-[v0]", views: onlineSwitch)
        
        view.addConstraintsWithVisualFormat(format: "V:|-100-[v0(50)]-10-[v1(50)]-10-[v2(50)]-50-[v3(50)]-100-[v4(50)]-10-[v5(50)]", views: truckIdTextField, emailTextField, passwordTextField, loginButton, onlineLabel, onlineSwitch)
    }
    
    @objc func handleLogin() {
        if emailTextField.text == "" || truckIdTextField.text == "" || passwordTextField.text == "" {
            self.createAlert(title: "Invalid entry", message: "Invalid email, password or truck Id.")
        } else {
            guard let password = passwordTextField.text else {return}
            guard let email = emailTextField.text else {return}
            if loginState == false {
                Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
                    if error != nil {
                        self.createAlert(title: "Invalid Entry", message: "Invalid email, password or truck Id")
                        return
                    } else {
                        self.loginButton.setTitle("Logoff", for: .normal)
                        self.onlineSwitch.isHidden = false
                        self.loginState = true
                    }
                })
            } else {
                self.loginButton.setTitle("Login", for: .normal)
                self.onlineSwitch.isHidden = true
                self.loginState = false
                self.status = "Offline"
//                removeEntryInOverwriteLocations()
                do {
                    try Auth.auth().signOut()
                } catch let error {
                    print("Could not logoff:", error)
                }
            }
            
        }
    }
    
    @objc func handleOnlineSwitch() {
        if onlineSwitch.isOn == true {
            self.onlineLabel.textColor = .green
            self.onlineLabel.text = "Online"
            self.onlineSwitch.isEnabled = true
            self.emailTextField.isEnabled = false
            self.passwordTextField.isEnabled = false
            self.truckIdTextField.isEnabled = false
            updateUserLocation()
        } else {
            self.onlineLabel.textColor = .red
            self.onlineLabel.text = "Offline"
            self.emailTextField.isEnabled = true
            self.passwordTextField.isEnabled = true
            self.truckIdTextField.isEnabled = true
            locationManager.stopUpdatingLocation()
            updateOnlineStatus()
        }
    }
    
    func createAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            //self.dismiss(animated: true, completion: nil)
            return
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func updateUserLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
}

